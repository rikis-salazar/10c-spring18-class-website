# List of activities and/or topics covered (by week)

**Week 1**

*   Class syllabus

    -   04/2. Talked about the [class syllabus][syllabus].

    > You should definitely **read it** (_in case you haven't already_).

*   Version control software (`git`).  

    -   04/4. A local repository with two files using CLI (_e.g._: `putty`).  

    > Git commands: `init`, `status`, `add`, `commit`, `log`, `branch`, and
    > `merge`.

    -   04/6. Attempt to "save" local laguna repository to cloud (bitbucket)
        failed (`ssh` not correctly set). Instead, local repository (chromebook)
        was _pushed_ to bitbucket; then the repository was cloned into _laguna_.

    > Git commands: `remote add`, `push`, `clone`.

[syllabus]: ../syllabus/syllabus-spring18.pdf

---

**Week 2**

*   Version control software (`git`).  

    -   04/9. My attempt to "not use the console (terminal)" failed miserably.
        However, `git gui` + `gitk` were used succesfully to perform `git`
        operations (_e.g.,_ `clone`, `push`, etc). Also, `git push --force ...`
        was used to sync local repo with [mostly empty] remote repo.

    > Git commands: `git gui`, `gitk`.

*   Pic 10B review: standard containers

    -   04/11. We discussed the "inner workings" of STL containers. Emphasis was
        placed on "how they work" _vs._ "how they are used". Your TA will
        complement this lecture with examples and/or other remarks.

    > Resources: see [this repository][repo-containers] for a summary of the
    > info presented during lecture.

    -   04/13. We spent time figuring out different ways to implement _hash
        tables_ as well as _deque_s.

    > Resources: see [this set of slides][hash-tables-10b] to review material
    > related to hash tables; whereas I do not have a set of slides detailing
    > the inner workings of _deque_s, later during the course (assignment 3),
    > you will implement a `ring queue` class that features some of the main
    > characteristics of a _deque_.

[repo-containers]: https://bitbucket.org/rikis-salazar/10c-stl-containers
[hash-tables-10b]: https://docs.google.com/presentation/d/e/2PACX-1vQvsJOFsP1iw8afT8PHoyRfrkC3HeH1-BaXJAF0HgxqH40lVvgpvIs0V0qKJwZfn983Hdy4rQVmPYLZ/pub?start=false&loop=false&delayms=3000

---

**Week 3**

*   Pic 10B review: inheritance & polymorphism 

    -   04/16. Discussed basic concepts related to inheritance in `c++`.

    > Resources: see [this repository][repo-inheritance] containing the examples
    > I presented during lecture.

    -   04/18. Continuation of inheritance in `c++`. Also, brief intro to
        function pointers.

    > Resources: see [this example][ptr-fun-example]. 

    -   04/20. Functors and Templates (revisited).

    > Resources: the whole lecture was _recorded_ via git commits. See [this
    > set of commits][in-lecture].

[repo-inheritance]: https://bitbucket.org/rikis-salazar/10c-review-inheritance
[ptr-fun-example]: http://cpp.sh/9gse
[in-lecture]: https://bitbucket.org/rikis-salazar/10c-spring18-class-website/branch/rough_notes

---
