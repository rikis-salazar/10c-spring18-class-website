# Makefile to manage the pic10c website

ROOT_D=         .
SYLL_D=         syllabus
SYLL_PDF=       $(SYLL_D)/syllabus-spring18.pdf
SYLL_MD=        $(SYLL_D)/readme.md
SYLL_RAW=       $(SYLL_D)/syllabus.txt
CSS_FILE=       $(ROOT_D)/pandoc.css

.PHONY: all debug update_syllabus

all: index.html

index.html: readme.md $(CSS_FILE)
	@echo Converting $< to $@
	@pandoc --css $(CSS_FILE) -o $@ $<


debug:
	@echo Directory: $(SYLL_D)
	@echo PDF: $(SYLL_PDF)
	@echo MD: $(SYLL_MD)
	@echo RAW: $(SYLL_RAW)
	@echo ROOT: $(ROOT_D)
	@echo CSS: $(CSS_FILE)


update_syllabus: $(SYLL_PDF) $(SYLL_RAW)


$(SYLL_PDF): $(SYLL_MD)
	@cd $(SYLL_D) && ./create_pdf.sh


$(SYLL_RAW): $(SYLL_MD)
	@cp $< $@ 
