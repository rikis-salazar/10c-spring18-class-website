# PIC 10C: Advanced Programming (spring 2018)

This is the "unofficial" class website. Here you will find handouts, slides, and
code related to the concepts we discuss during lecture.

---

## Lecture room change

Starting on Wednesday, April 25, we **will no longer be meeting at MS 5118**;
instead, we **will meet at MS 5147**, which is a slightly bigger room.

> **Note:** For the time being, **this change does not apply** to discussion
> sessions.

---

## Class contents

 *  Syllabus

    -   [PDF version][syll-pdf]
    -   [Bitbucket formatted Markdown version][syll-bb-md]
    -   [`raw` Markdown version][syll-raw-md]

 *  [Topics and resources][topics]

 *  Homework assignments 

    -  [Assignment 1:][hw1] Version control.
    -  [Assignment 2:][hw2] A `Qt` grade calculator.
    -  [Assignment 3:][hw3] A ring queue.

 *  [Lectures][lectures]

[syll-pdf]: syllabus/syllabus-spring18.pdf
[syll-bb-md]: https://bitbucket.org/rikis-salazar/10c-spring18-class-website/src/master/syllabus/readme.md
[syll-raw-md]: syllabus/syllabus.txt
[topics]: topics/
[hw1]: assignments/hw1/
[hw2]: assignments/hw2/
[hw3]: assignments/hw3/
[lectures]: lectures/

---

## Class websites

During this transition period, while moving away from CCLE, material will be
hosted mostly at the _laguna_ (PIC) math server. However, on occasions you might
still be referred back to the "official" CCLE website and/or other external
sites. This arrange might lead to confusion, but in essence everything you need
should be accessible via the following links:

 *  [The "official" CCLE website][CCLE]:

    > Use it to _submit_ your assignments, read class announcements, locate
    > information about O.H., etc.

 *  [The "unofficial" PIC website][PIC]:

    > For _handouts_, _slides_, `code`, and other resources.

 *  [The `10c-spring18-class-website` repository][BB-SP18]:

    > A mirror of the "unofficial" PIC website. It might be a little harder to
    > navigate, but you get access to the inner workings of this particular
    > setup.

[CCLE]: https://ccle.ucla.edu/course/view/18S-COMPTNG10C-1
[PIC]: https://pic.ucla.edu/~rsalazar/pic10c/
[BB-SP18]: https://bitbucket.org/rikis-salazar/10c-spring18-class-website

Also, at times you might be referred to other bitbucket repositories. Most of
them can be located via [this link][bb-search]. Use the search feature (_e.g.,_
containers, RAII, smart pointers, etc.) to quickly locate particular
repositories.

> **Note:** Some of the repos hosted therein are _deprecated_. In other words,
> I no longer maintain them, and they might contain mistakes ranging from
> insignificant typos, to complete blunders.

[bb-search]: https://bitbucket.org/rikis-salazar/
