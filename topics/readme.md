# Topics discussed during lecture (up to week 2)

## Pic10B (review)

While covering this material, emphasis is placed in the
_this-is-how-things-work-under-the-hood_ side compared to the
_this-is-how-you-use-it_ side. The idea here is to provide you withe the
information you need (_e.g.,_ requirements, complexity, etc.) to decide which
particular tool suits your needs best; it should be easy then to locate
appropriate documentation (:cough: :cough: _Google it!_ :cough: :cough:), as
well as examples of how said tools are used in practice.

 *  [The `c++` Standard Template Library (containers)][containers]: a short
    summary of important information.

> Confused about _this-is-how-things-work..._ vs. _this-is-how-you-use-it_? Take
> a look at the two links below. They tackle different sides for hash tables
> (`unordered_map`).
>
>  *  [`unordered_map` usage tutorial and example][um-example]: Result of a
>     simple Google search.
>  *  [Introduction to Hash Tables][pic10b-lesson12]: A few years ago, I used to
>     cover _Hash Tables_ in my Pic10b class. Nowadays I favor other topics
>     instead.
>
>     **Warning:** This material is both _incomplete_ and _deprecated_;
>     regardless, it should give a good idea about the pros and cons of hash
>     tables.

[containers]: https://bitbucket.org/rikis-salazar/10c-stl-containers
[pic10b-lesson12]: https://docs.google.com/presentation/d/e/2PACX-1vQvsJOFsP1iw8afT8PHoyRfrkC3HeH1-BaXJAF0HgxqH40lVvgpvIs0V0qKJwZfn983Hdy4rQVmPYLZ/pub?start=false&loop=false&delayms=3000
[um-example]: http://thispointer.com/unordered_map-usage-tutorial-and-example/

---

## Version control

We have toyed around with local repositories, as well as a couple of remote ones
(hosted on [bitbucket][my-bb]). While managing local repositories I have mostly
used a _command line interface_ (CLI for short), but have also showcased some
rudimentary _graphical user interfaces_ (GUI's for short). 

Here is a list of resources you might want to consider checking out at some
point:

 *  [Bitbucket][BB] & [Github][GH]
 *  [Sourcetree][ST] & [Github][GHD]
 *  [Git tutorial (`github`)][tutorial-github]
 *  [Learn SourceTree with BitBucket Cloud (bitbucket)][LST-BBC]
 *  [GitHub Desktop User Guides][GHD-UG]
 *  [Hello World in GitHub][HW-GH]
 *  [An Intro to Git and GitHub for Beginners][IntroGit]

[my-bb]: https://bitbucket.org/rikis-salazar
[BB]: https://bitbucket.org/
[GH]: https://github.com/
[ST]: https://www.sourcetreeapp.com/
[GHD]: https://desktop.github.com/
[tutorial-github]: https://try.github.io/levels/1/challenges/1
[LST-BBC]: https://confluence.atlassian.com/bitbucket/tutorial-learn-sourcetree-with-bitbucket-cloud-760120235.html
[GHD-UG]: https://help.github.com/desktop/guides/
[HW-GH]: https://guides.github.com/activities/hello-world/
[IntroGit]: http://product.hubspot.com/blog/git-and-github-tutorial-for-beginners

> If you decide that GUI's are not for you, then I highly recommend you bookmark
> some of the following links:
>
>  *  [Git Tutorial (local branching on the cheap)][branch-cheap]. By the very
>     same developers of `git`.
>  *  [Git FAQ][git-faq]. For specific, everyday type of examples.
>  *  [Pro Git (book)][git-book]. In case you want to be a _Git-Fu_ master.  
>     Also available in several downloadable formats:
>
>     - [PDF][book-pdf]
>     - [EPUB][book-epub]
>     - [MOBI][book-mobi]

[branch-cheap]: https://git-scm.com/docs/gittutorial
[git-faq]: http://gitfaq.org/
[git-book]: https://git-scm.com/book/en/v2
[book-pdf]: https://progit2.s3.amazonaws.com/en/2016-03-22-f3531/progit-en.1084.pdf
[book-epub]: https://progit2.s3.amazonaws.com/en/2016-03-22-f3531/progit-en.1084.epub
[book-mobi]: https://progit2.s3.amazonaws.com/en/2016-03-22-f3531/progit-en.1084.mobi

